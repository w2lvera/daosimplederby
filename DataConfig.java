/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.daosimpleanotation.config;

/**
 *
 * @author Wera
 */
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@Configuration
@ComponentScan(value=("com.vera.daosimpleanotation.logical"),value={"com.journaldev.spring.di.consumer"})
//@ComponentScan("com.vera.daosimpleanotation.dao")
@PropertySource("classpath:database.properties")
public class DataConfig {
    @Autowired
	Environment environment;

	private final String URL = "url";
	private final String USER = "parol";
	private final String DRIVER = "driver";
	private final String PASSWORD = "password";

	@Bean
	DataSource dataSource() {
		DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
		driverManagerDataSource.setUrl(environment.getProperty(URL));
		driverManagerDataSource.setUsername(environment.getProperty(USER));
		driverManagerDataSource.setPassword(environment.getProperty(PASSWORD));
		driverManagerDataSource.setDriverClassName(environment.getProperty(DRIVER));
		return driverManagerDataSource;
        }
}       @Bean
	public DataSource getDataSource(){
		return new CustomerDao();
	}
        @Bean
	public com.journaldev.spring.di.consumer.CustomerDao getCustomerDao(){
		return new com.journaldev.spring.di.consumer.CustomerDao();
	}
        
        
}
