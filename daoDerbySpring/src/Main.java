
import com.vera.daoderbyxml.entity.Customer;
import com.vera.daoderbyxml.logical.CustomerListProcessor;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.sql.DriverManager;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Wera
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        Properties defaultProps = new Properties();
        defaultProps.load(Main.class.getResourceAsStream("test.properties"));
        ApplicationContext context = new ClassPathXmlApplicationContext("SpringXMLConfig.xml");
        CustomerListProcessor processor = (CustomerListProcessor) context.getBean("customerProcessor");
        String result = "";
        ArrayList<Customer> list = (ArrayList) processor.getCustomerList("J");
        for (Customer x : list) {
            result += x + "\n";
        }
        System.out.println(result);
    }
}
