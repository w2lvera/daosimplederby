/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.daoderbyspring.dao;

import com.vera.daoderbyspring.entity.Customer;
import java.sql.Connection;
import java.util.List;
import javax.sql.DataSource;

/**
 *
 * @author Wera
 */
public interface CustomerDao {
    void insertCustomer(Customer t);
    void setDataSource(DataSource c);
    Customer getCustomerByName(String name);
    List<Customer> getAllCustomer();
    List<Customer> getICostomer(String s);
   
    
}
