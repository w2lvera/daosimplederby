/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.daoderbyspring.config;


import com.vera.daoderbyspring.dao.CustomerDao;
import com.vera.daoderbyspring.logical.CustomerListProcessor;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Component;

/**
 *
 * @author Wera
 */
@Configuration
@PropertySource("classpath:database.properties")

public class AppConfig {
    @Autowired
    Environment environment;

	private final String URL = "url";
	private final String USER = "parol";
	private final String DRIVER = "driver";
	private final String PASSWORD = "password";

	@Bean
	DataSource dataSource() {
		DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
		driverManagerDataSource.setUrl(environment.getProperty(URL));
		driverManagerDataSource.setUsername(environment.getProperty(USER));
		driverManagerDataSource.setPassword(environment.getProperty(PASSWORD));
		driverManagerDataSource.setDriverClassName(environment.getProperty(DRIVER));
                           
		return driverManagerDataSource;
        }
        @Bean
//        <bean id="customerDao" class="com.vera.daoderbyxml.dao.CustomerDaoImpl">
//		<property name="dataSource" ref="dataSource" />
        CustomerDao customerDao(){
            com.vera.daoderbyspring.dao.CustomerDao customerDao = new com.vera.daoderbyspring.dao.CustomerDaoImpl();
            customerDao.setDataSource(dataSource());
            return customerDao;
        }
        @Bean
//       <bean id="customerProcessor" class="com.vera.daoderbyxml.logical.CustomerListProcessorImpl">
//       <property name="customerDao"  ref="customerDao"/>
        CustomerListProcessor customerProcessor(){
                com.vera.daoderbyspring.logical.CustomerListProcessor customerListProcessor =
                new com.vera.daoderbyspring.logical.CustomerListProcessorImpl();
        customerListProcessor.setCustomerDao (customerDao());
        return customerListProcessor;
        }
}
