/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.daoderbyspring;

import com.vera.daoderbyspring.entity.Customer;
import com.vera.daoderbyspring.logical.CustomerListProcessor;
import java.io.IOException;
import java.util.ArrayList;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author Wera
 */
public class CustomerXMLApplication {
    public static void main(String[] args) throws IOException {
    ApplicationContext context = new ClassPathXmlApplicationContext("SpringXMLConfig.xml");
        CustomerListProcessor processor = (CustomerListProcessor) context.getBean("customerProcessor");
        String result = "";
        ArrayList<Customer> list = (ArrayList) processor.getCustomerList("J");
        for (Customer x : list) {
            result += x + "\n";
        }
        System.out.println(result);
    }
}
