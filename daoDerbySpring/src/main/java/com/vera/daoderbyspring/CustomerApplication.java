/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.daoderbyspring;

import com.vera.daoderbyspring.config.AppConfig;
import com.vera.daoderbyspring.entity.Customer;
import com.vera.daoderbyspring.logical.CustomerListProcessor;
import java.util.ArrayList;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 *
 * @author Wera
 */
public class CustomerApplication {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
         ApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig.class);
        CustomerListProcessor processor =  ctx.getBean(CustomerListProcessor.class);
        String result = "";
        ArrayList<Customer> list = (ArrayList) processor.getCustomerList("J");
        for (Customer x : list) {
            result += x + "\n";
        }
        System.out.println(result);

    }
    
}
