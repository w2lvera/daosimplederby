/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.daoderby.dao;

import com.vera.daoderby.entity.Customer;
import java.sql.Connection;
import java.util.List;

/**
 *
 * @author Wera
 */
public interface CustomerDao {
    void insertCustomer(Customer t);
    void setDataSourse(Connection c);
    Customer getCustomerByName(String name);
    List<Customer> getAllCustomer();
    List<Customer> getICostomer(String s);
   
    
}
