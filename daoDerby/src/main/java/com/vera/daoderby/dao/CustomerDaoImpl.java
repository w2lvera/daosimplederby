/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.daoderby.dao;

import com.vera.daoderby.entity.Customer;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Wera
 */
public class CustomerDaoImpl implements CustomerDao {

    private Connection dataSource;

    @Override
    public void setDataSourse(Connection c) {
        this.dataSource = c;
    }

    @Override
    public Customer getCustomerByName(String name) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void insertCustomer(Customer t) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<Customer> getAllCustomer() {
        String sql = "SELECT * FROM Customer";
        Connection conn = null;
        try {
            conn = dataSource;
            PreparedStatement ps = conn.prepareStatement(sql);
            List<Customer> customers = new ArrayList<Customer>();
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Customer town = new Customer(
                        rs.getInt("CUSTOMER_ID"),
                        rs.getString("NAME")
                );
                customers.add(town);
            }
            rs.close();
            ps.close();
            return customers;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                }
            }
        }
    }

    @Override
    public List<Customer> getICostomer(String s) {
        Connection conn = null;

        try {
            conn = dataSource;//.getConnection();
            PreparedStatement ps = conn.prepareStatement(s);
            List<Customer> customers = new ArrayList<Customer>();
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Customer town = new Customer(
                        rs.getInt("CUSTOMER_ID"),
                        rs.getString("NAME")
                );
                customers.add(town);
            }
            rs.close();
            ps.close();
            return customers;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                }
            }
        }
    }
}
