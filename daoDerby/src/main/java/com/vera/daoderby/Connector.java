/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.daoderby;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

/**
 *
 * @author Wera
 */
public class Connector {
    protected Connection connection = null;

    public Connector() {
    }

    public Connection getConnection() {
        return connection;
    }

    public void initialize() throws IOException{
      
    Properties defaultProps = new Properties();
    defaultProps.load(Connector.class.getResourceAsStream("/test.properties"));

    String url=defaultProps.getProperty("url");
    String pasport =defaultProps.getProperty("pasvord") ;
    String driver =defaultProps.getProperty("driver") ;
    String parol = defaultProps.getProperty("parol");

	  try{
              System.out.println("Start");
              Class.forName(driver);//("org.apache.derby.jdbc.ClientDriver");
          }
          catch(Exception e) { 
              System.out.println("Class def not found: " + e);
          }
	
          try {        
		  connection = DriverManager.getConnection(url,pasport,parol);//, username, password);  
                  
	  }
	  catch (Throwable theException){theException.printStackTrace();
          System.out.println("Error");
          }
  }
}
