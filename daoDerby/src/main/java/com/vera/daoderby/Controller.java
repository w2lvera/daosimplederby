/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.daoderby;

import com.vera.daoderby.dao.CustomerDao;
import com.vera.daoderby.dao.CustomerDaoImpl;
import com.vera.daoderby.entity.Customer;
import com.vera.daoderby.logical.CustomerListProcessorImpl;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Wera
 */
public class Controller {
    Connector connection;
    CustomerListProcessorImpl customerListProcessorImpl;
    CustomerDao customerDao;

    public Controller() {
        connection = new Connector();
        try {
            connection.initialize();
        } catch (IOException ex) {
            System.out.println("no connection");
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
        customerDao  = new CustomerDaoImpl();
        customerDao.setDataSourse(connection.getConnection());
        customerListProcessorImpl = new CustomerListProcessorImpl();
        customerListProcessorImpl.setCustomerDao(customerDao);
    }
    public List<Customer> getListCustomer(String s){
         return customerListProcessorImpl.getCustomerList(s);
    }
    public void printList(String prefix){
        String result = "";
        ArrayList<Customer> list = (ArrayList)getListCustomer(prefix);
        for(Customer x:list)result+= x+"\n";
        System.out.println(result);
    }
    
}
