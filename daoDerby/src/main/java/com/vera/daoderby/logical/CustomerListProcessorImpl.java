/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vera.daoderby.logical;

import com.vera.daoderby.dao.CustomerDao;
import com.vera.daoderby.entity.Customer;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Wera
 */
public class CustomerListProcessorImpl implements CustomerListProcessor {

    @Override
    public List<Customer> getCustomerList(String prefix) {
        final List<Customer> allCustomers = customerDao.getAllCustomer();
        final List<Customer> result = new ArrayList<Customer>();
        for (Customer customer : allCustomers) {
            if (customer.getName().startsWith(prefix)) {
                result.add(customer);
            }
        }
        return result;
    }
    private CustomerDao customerDao;

    public void setCustomerDao(CustomerDao customerDao) {
        this.customerDao = customerDao;
    }

}
