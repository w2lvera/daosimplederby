package com.vera.daoderby.logical;

import com.vera.daoderby.entity.Customer;
import java.util.List;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Wera
 */
public interface CustomerListProcessor {
    List<Customer> getCustomerList(String prefix);
}

